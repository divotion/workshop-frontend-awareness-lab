(function (){
    'use strict';

    angular
        .module('app', [
            'ui.router'
        ]);

})();

(function (){
    'use strict';

    const CONFIG = {
        WEATHER_URL: 'http://api.openweathermap.org/data/2.5/weather?APPID=a00496ac0ec72b4d59f494abaa1472c3&q='
    };


    angular
        .module('app')
        .constant('CONFIG', CONFIG);
})();

(function () {
    'use strict';


    const AppComponent = {
        template: `
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                                aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" ui-sref="home">Example project</a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="active" ui-sref="welcome">Welcome</a></li>
                            <li><a ui-sref="weather">Weather</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            
            <div class="container">
                <div class="main-view" ui-view>
                </div>
            </div>
        `
    };


    angular
        .module('app')
        .component('feaApp', AppComponent);

})();

(function () {
    'use strict';

    angular
        .module('app')
        .config(($stateProvider, $urlRouterProvider) => {
            'ngInject';

            $stateProvider
                .state('welcome', {
                    url: '/welcome',
                    component: 'feaWelcome'
                });

            $urlRouterProvider.otherwise('/welcome');
        });

})();

(function () {
    'use strict';

    const WelcomeComponent = {
        template: `
            <h3 id="greeting">{{$ctrl.greeting}}</h3>
        `,

        controller: class WelcomeComponent {
            constructor() {
                this.greeting = 'Hello Frontend World!';
            }
        }

    };


    angular
        .module('app')
        .component('feaWelcome', WelcomeComponent);

})();

(function () {
    'use strict';

    angular
        .module('app')
        .config(($stateProvider) => {
            'ngInject';

            $stateProvider
                .state('weather', {
                    url: '/weather',
                    component: 'feaWeather'
                });
        });

})();

(function () {
    'use strict';

    let beaufort = [0.3, 1.5, 3.3, 5.5, 8, 10.8, 13.9, 17.2, 20.7, 24.5, 28.4, 32.6];
    let directions = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];


    class WeatherService {
        constructor($http, CONFIG) {
            'ngInject';
            this.$http = $http;
            this.config = CONFIG;
        }

        static toBeaufort(mps) {
            for (let i = 0; i < beaufort.length; i++) {
                if (mps < beaufort[i]) {
                    return i;
                }
            }

            return 12;
        }

        static toDirection(degrees) {
            degrees += 11.25;
            if (degrees > 360) {
                degrees -= 360;
            }
            let index = Math.floor(degrees / (360 / directions.length));

            return directions[index];
        }


        weather(city) {
            return this.$http.get(this.config.WEATHER_URL + city)
                .then((response) => {
                    return {
                        name: response.data.name,
                        main: {
                            temp: response.data.main.temp - 273.15,
                            humidity: response.data.main.humidity
                        },
                        coord: response.data.coord,
                        sunrise: new Date(response.data.sys.sunrise * 1000),
                        sunset: new Date(response.data.sys.sunset * 1000),
                        wind: {
                            speed: response.data.wind.speed,
                            degrees: response.data.wind.deg,
                            beaufort: WeatherService.toBeaufort(response.data.wind.speed),
                            direction: WeatherService.toDirection(response.data.wind.deg)
                        }
                    };
                });
        }
    }


    angular
        .module('app')
        .service('WeatherService', WeatherService);

})();

(function () {
    'use strict';

    const WeatherComponent = {
        template: `
            <div>
                <input ng-model="$ctrl.newLocation">
                <button type="button" ng-click="$ctrl.addLocation()">Add location</button>
            </div>
            
            <hr />
            
            <div ng-repeat="location in $ctrl.locations">
                <span class="weather__location">
                    <fea-weather-city city="{{location}}"></fea-weather-city>
                </span>
                <span class="weather__remove"
                      ng-click="$ctrl.removeLocation($index)">
                    X
                </span>
            </div>        
        `,

        controller: class WeatherComponent {
            $onInit() {
                this.locations = ['The Hague, NL', 'Utrecht, NL'];
            }

            addLocation() {
                this.locations.push(this.newLocation);
                this.newLocation = '';
            }

            removeLocation(index) {
                this.locations.splice(index, 1);
            }
        }

    };

    angular
        .module('app')
        .component('feaWeather', WeatherComponent);

})();

(function () {
    'use strict';

    const WeatherCityComponent = {
        template: `
            <div ng-if="!$ctrl.isLoading">
                <h2>Weather in <a href="https://www.google.nl/maps/@{{$ctrl.local.coord.lat}},{{$ctrl.local.coord.lon}},15z">{{$ctrl.city}}</a>
                </h2>
                <p>Temperature: {{$ctrl.local.main.temp | number : 1}}&deg;C Humidity: {{$ctrl.local.main.humidity}}%</p>
                <p>Wind: {{$ctrl.local.wind.beaufort}} Beaufort ({{$ctrl.local.wind.speed}}ms/s) Direction:
                    {{$ctrl.local.wind.direction}} ({{$ctrl.local.wind.degrees | number : 0}}&deg;)</p>
                <p>Lattitude: {{$ctrl.local.coord.lat}}, longitude: {{$ctrl.local.coord.lon}}</p>
                <p>Sunrise: {{$ctrl.local.sunrise | date : 'HH:mm'}} Sunset: {{$ctrl.local.sunset | date : 'HH:mm'}}</p>
            </div>
            <div ng-if="$ctrl.isLoading">Loading...</div>
        `,

        bindings: {
            city: '@'
        },

        controller: class WeatherCityComponent {
            constructor(WeatherService) {
                'ngInject';
                this.WeatherService = WeatherService;
            }

            $onInit() {
                this.isLoading = true;

                this.WeatherService.weather(this.city)
                    .then((response) => {
                        this.local = response;
                    })
                    .finally(() => this.isLoading = false);
            }
        }

    };


    angular
        .module('app')
        .component('feaWeatherCity', WeatherCityComponent);

})();
